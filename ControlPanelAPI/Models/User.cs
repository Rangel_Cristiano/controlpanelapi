﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Coletor.Models
{
    [Table("users")]

    public class User
    {
        [Key]
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public string Specialist { get; set; }
        public DateTime Created { get; set; }
        public int Admin { get; set; }
    }
}
