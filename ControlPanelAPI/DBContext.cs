﻿using Coletor.Configuration;
using Coletor.Models;
using Microsoft.EntityFrameworkCore;

namespace Coletor
{
    public class DBContext : DbContext
    {   
        public DbSet<User> User { get; set; }

        public DBContext(DbContextOptions<DBContext> options) :
            base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {       
            modelBuilder.ApplyConfiguration(
                new UserConfiguration());
        }
    }
}