﻿using System;
using System.Linq;
using Coletor;
using Coletor.Models;
using ControlPanelAPI.Models;
using Microsoft.AspNetCore.Mvc;

namespace ControlPanelAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminsController : ControllerBase
    {
        // GET api/admins/email
        [HttpGet("{email}")]
        public ActionResult Get(
            [FromServices]DBContext dbContext,
            String email)
        {
            var user = dbContext.User.FirstOrDefault(u => u.Admin == 1 && u.Email.Equals(email));

            if (user == null)
                return Ok(null);

            Admin admin = new Admin();
            admin.Id = user.Id;
            admin.Email = user.Email;
            admin.Password = user.Password;

            return Ok(admin);
        }
    }
}