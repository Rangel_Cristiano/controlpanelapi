﻿using System;
using System.Linq;
using Coletor;
using Coletor.Models;
using Microsoft.AspNetCore.Mvc;

namespace ControlPanelAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        // GET api/users
        [HttpGet]
        public ActionResult Get([FromServices]DBContext dbContext)
        {
            return Ok(dbContext.User);
        }

        // GET api/users/5
        [HttpGet("{id}")]
        public ActionResult Get(
            [FromServices]DBContext dbContext,
            int id)
        {
            var user = dbContext.User.Where(u => u.Id == id).First();

            return Ok(user);
        }

        // POST api/users
        [HttpPost]
        public void Post(
            [FromServices]DBContext dbContext, 
            User user)
        {
            user.Id = null;
            user.Created = DateTime.Now;
       
            dbContext.User.Add(user);
            dbContext.SaveChanges();
        }

        // PUT api/users/5
        [HttpPut("{id}")]
        public ActionResult Put(
            [FromServices]DBContext dbContext,
            int id, 
            [FromBody] User user)
        {
            User u = dbContext.User.Where(s => s.Id == id).First();
            u.Email = user.Email;
            u.Name = user.Name;
            u.Phone = user.Phone;
            u.Specialist = user.Specialist;
            dbContext.SaveChanges();

            return Ok(u);
        }

        // DELETE api/users/5
        [HttpDelete("{id}")]
        public bool Delete(
               [FromServices]DBContext dbContext,
               int id)
        {
            try
            {
                User user = dbContext.User.Where(s => s.Id == id).First();
                dbContext.User.Remove(user);
                dbContext.SaveChanges();

                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }
    }
}
