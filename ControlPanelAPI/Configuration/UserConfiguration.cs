﻿using Coletor.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Coletor.Configuration
{
    public class UserConfiguration : 
        IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> entity)
        {
            entity.Property(e => e.Id)
                .HasColumnName("id");
                 
            entity.Property(e => e.Created)
                .HasColumnName("created")
                .HasDefaultValueSql("CURRENT_TIMESTAMP");

            entity.Property(e => e.Email)
                .IsRequired()
                .HasColumnName("email")
                .HasMaxLength(255)
                .IsUnicode(false);

            entity.Property(e => e.Name)
                .IsRequired()
                .HasColumnName("name")
                .HasMaxLength(255)
                .IsUnicode(false);

            entity.Property(e => e.Password)
                .IsRequired()
                .HasColumnName("password")
                .HasMaxLength(255)
                .IsUnicode(false);

            entity.Property(e => e.Phone)
                .IsRequired()
                .HasColumnName("phone")
                .HasMaxLength(15)
                .IsUnicode(false);

            entity.Property(e => e.Specialist)
                .IsRequired()
                .HasColumnName("specialist")
                .HasMaxLength(255)
                .IsUnicode(false);

            entity.Property(e => e.Admin)
            .HasColumnName("admin");
        }
    }
}